package facci.baquejorge.convertidor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    TextView textparacovertirvalor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textparacovertirvalor = (TextView) findViewById(R.id.textparacovertirvalor);
        String bundle = this.getIntent().getExtras().getString("Libras");
        Double gramos = Double.valueOf(bundle)*453.592;
        textparacovertirvalor.setText(gramos.toString());

        Toast.makeText(getApplicationContext(),"La convercion es", Toast.LENGTH_SHORT).show();
        Log.e("mensaje","La convercion es ");




    }
}

